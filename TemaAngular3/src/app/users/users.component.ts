import { Component, OnInit } from '@angular/core';
import { User } from '../user';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: User[] = [{ id: 1, name: 'John', email: 'jhonedoe@e.com', phone: 0, address: 'lorem ipsum', website: 'j.com' },
  { id: 2, name: 'Marcus', email: 'Marcus@e.com', phone: 0, address: 'lorem ipsum', website: 'M.com' },
  { id: 3, name: 'Alex', email: 'alex@e.com', phone: 0, address: 'lorem ipsum', website: 'A.com' }];
  user: User = { id: 0, name: '', email: '', phone: 0, address: '', website: '' };

  constructor() { }

  ngOnInit(): void {
  }

  OnSearch(nume: string): void {
    for (let i = 0; i < this.users.length; i++) {
      if (nume == this.users[i].name) {
        this.user = this.users[i];
      }
    }
  }
}
